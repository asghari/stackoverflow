__author__ = 'Destiny'
from wsgiref.simple_server import make_server  # for server
from pyramid.config import Configurator  # for config pyramid project
from settings import settings  # setting
from urls import urls_pattern  # url
from pyramid_redis_sessions import session_factory_from_settings  # session


# create object of configurator
config = Configurator(settings=settings)

# for redis
session_factory = session_factory_from_settings(settings)
config.set_session_factory(session_factory)

config.add_static_view(name='static', path=':static')
config.add_static_view(name='templates', path=':templates')
config.add_jinja2_search_path('templates')

# add route/ the url
for route, url in zip(urls_pattern.keys(), urls_pattern.values()):
    config.add_route(route, url)

# scanning views
config.scan('views')


""" create a web server / host name && port && ... """
app = config.make_wsgi_app()

# the start / main
if __name__ == '__main__':
    server = make_server('127.0.0.1', 8889, app)
    server.serve_forever()

