from peewee import *

database = MySQLDatabase("stackoverflow", host="127.0.0.1", port=3306, user="root", passwd="")


class MyModel(Model):
    class Meta:
        database = database


class Type(MyModel):
    id = PrimaryKeyField()
    name = CharField()


class Province(MyModel):
    id = PrimaryKeyField()
    name = CharField()


class Users(MyModel):
    id = PrimaryKeyField()
    username = CharField()
    password = CharField()
    name = CharField()
    family = CharField()
    email = CharField()
    img_name = CharField()
    province = ForeignKeyField(Province, to_field=Province.id)


class UserType(MyModel):
    id = PrimaryKeyField()
    users = ForeignKeyField(Users, to_field=Users.id)
    type = ForeignKeyField(Type, to_field=Type.id)


class Qresponse(MyModel):
    id = PrimaryKeyField()
    content = TextField()
    like = IntegerField()
    state = IntegerField()
    users = ForeignKeyField(Users, to_field=Users.id)
    date = DateTimeField()
    comment_count = IntegerField()
    view_count = IntegerField()


class Qresponset(MyModel):
    id = PrimaryKeyField()
    content = TextField()
    state = IntegerField()
    users = ForeignKeyField(Users, to_field=Users.id)


class Tags(MyModel):
    id = PrimaryKeyField()
    name = CharField()


class Qtags(MyModel):
    id = PrimaryKeyField()
    tags = ForeignKeyField(Tags, to_field=Tags.id)
    qresponse = ForeignKeyField(Qresponse, to_field=Qresponse.id)


class Qtagst(MyModel):
    id = PrimaryKeyField()
    tags = ForeignKeyField(Tags, to_field=Tags.id)
    qresponset = ForeignKeyField(Qresponset, to_field=Qresponset.id)


class Likes(MyModel):
    id = PrimaryKeyField()
    users = ForeignKeyField(Users, to_field=Users.id)
    qresponse = ForeignKeyField(Qresponse, to_field=Qresponse.id)
    state = IntegerField()
