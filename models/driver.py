import datetime

from models.tabels import *


class MyUser:
    def __init__(self, id_=None, username=None, password=None, name=None, family=None, email=None,
                 img_name=None, province=None):
        self.id = id_
        self.username = username
        self.password = password
        self.name = name
        self.family = family
        self.email = email
        self.img_name = img_name
        self.province = province

    def select_all(self):
        res_db = Users().select()
        li_ = []
        for dic_ in res_db:
            li_.append(
                {"name": dic_.name, "family": dic_.family, "username": dic_.username, "province": dic_.province.name,
                 "img_name": dic_.img_name})
        return li_

    def check_user(self):
        try:
            dic_db = Users().get(Users.username == self.username, Users.password == self.password)
            dic_ = {"id": dic_db.id, "img_name": dic_db.img_name, "username": dic_db.username}
            return dic_
        except:
            return {}

    def insert_user(self):
        try:
            user_id = Users().insert(name=self.name, family=self.family, email=self.email, username=self.username,
                                     password=self.password, img_name=self.img_name, province=self.province).execute()
            return user_id
        except:
            return False

    def select_user_info(self):
        try:
            dic_db = Users().get(Users.id == self.id)
            dic_ = {"name": dic_db.name, "family": dic_db.family, "email": dic_db.email, "username": dic_db.username,
                    "password": dic_db.password}
            return dic_
        except:
            return {}

    def update_user_info(self):
        Users().update(name=self.name, family=self.family, username=self.username, password=self.password,
                       email=self.email).where(Users.id == self.id).execute()
        return True


class MyUsertype:
    def __init__(self, type=None, users=None):
        self.type = type
        self.users = users

    def insert(self):
        UserType().insert(type=self.type, users=self.users).execute()
        return True

    def select_types_user(self):
        li_db = UserType().select().where(UserType.users == self.users).execute()
        li_ = []
        for dic_ in li_db:
            li_.append({"user_type": dic_.type.name})
        return li_


class MyQresponse:
    def __init__(self, id_=None, content=None, like=0, state=0, users=None, date=datetime.datetime.now(),
                 comment_count=0, view_count=0):
        self.id = id_
        self.content = content
        self.like = like
        self.state = state
        self.users = users
        self.datetime = date
        self.comment_count = comment_count
        self.view_count = view_count

    def update_likes(self):
        Qresponse().update(like=self.like).where(Qresponse.id == self.id).execute()
        return True

    def update_comments(self, q_id):
        db_dic = Qresponse().get(Qresponse.id == q_id)
        Qresponse().update(comment_count=db_dic.comment_count + 1).where(Qresponse.id == q_id).execute()
        return True

    def select_one(self):
        res_db = Qresponse().get(id=self.id)
        dic_ = {"content": res_db.content, "id": res_db.id}
        return dic_

    def insert_(self):
        try:
            id_q = Qresponse().insert(content=self.content, like=self.like, state=self.state,
                                      users=self.users, date=self.datetime, comment_count=self.comment_count,
                                      view_count=self.view_count).execute()
            return id_q
        except:
            return False

    def select_s_mahbob(self):
        dic_ = {}
        list_db = Qresponse().select().where(Qresponse.state == 0).order_by(Qresponse.like.desc()).paginate(
            page=1, paginate_by=5)

        print(list_db)
        for dic2_ in list_db:
            i = str(dic2_.id)
            dic_[i] = {}
            dic_[i]['content'] = dic2_.content
            dic_[i]['date'] = dic2_.date
            dic_[i]['user_info'] = {"username": dic2_.users.username, "img_name": dic2_.users.img_name}
            dic_[i]['like'] = dic2_.like
            dic_[i]['comment_count'] = dic2_.comment_count
            dic_[i]['view_count'] = dic2_.view_count
            dic_[i]['list'] = []
            li_2 = Qtags().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtags.tags)).where(
                Qtags.qresponse == int(i))
            dic_[i]['list'] = [{"name": di_.tags.name} for di_ in li_2]
        return dic_

    def select_s_jadid(self):
        dic_ = {}
        list_db = Qresponse().select().where(Qresponse.state == 0).order_by(Qresponse.id.desc()).paginate(page=1,
                                                                                                          paginate_by=5)
        for dic2_ in list_db:
            i = str(dic2_.id)
            dic_[i] = {}
            dic_[i]['content'] = dic2_.content
            dic_[i]['date'] = dic2_.date
            dic_[i]['like'] = dic2_.like
            dic_[i]['comment_count'] = dic2_.comment_count
            dic_[i]['view_count'] = dic2_.view_count
            dic_[i]['list'] = []
            li_2 = Qtags().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtags.tags)).where(
                Qtags.qresponse == int(i))
            dic_[i]['list'] = [{"name": di_.tags.name} for di_ in li_2]
        return dic_

    def select_s_all(self):
        dic_ = {}
        list_db = Qresponse().select().where(Qresponse.state == 0).order_by(Qresponse.id.desc())

        for dic2_ in list_db:
            i = str(dic2_.id)
            dic_[i] = {}
            dic_[i]['content'] = dic2_.content
            dic_[i]['date'] = dic2_.date
            dic_[i]['user_info'] = {"username": dic2_.users.username, "img_name": dic2_.users.img_name}
            dic_[i]['like'] = dic2_.like
            dic_[i]['comment_count'] = dic2_.comment_count
            dic_[i]['view_count'] = dic2_.view_count
            dic_[i]['list'] = []
            li_2 = Qtags().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtags.tags)).where(
                Qtags.qresponse == int(i))
            dic_[i]['list'] = [{"name": di_.tags.name} for di_ in li_2]
        return dic_

    def select_info_5_jadid(self):
        dic_ = {}
        list_db = Qresponse().select().where(Qresponse.state == 0).order_by(Qresponse.id.desc())
        for dic2_ in list_db:
            i = str(dic2_.id)
            dic_[i] = {}
            dic_[i]['content'] = dic2_.content
            dic_[i]['date'] = dic2_.date
            dic_[i]['user_info'] = {"username": dic2_.users.username, "img_name": dic2_.users.img_name}
            dic_[i]['like'] = dic2_.like
            dic_[i]['comment_count'] = dic2_.comment_count
            dic_[i]['view_count'] = dic2_.view_count
            dic_[i]['list'] = []
            li_2 = Qtags().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtags.tags)).where(
                Qtags.qresponse == int(i))
            dic_[i]['list'] = [{"name": di_.tags.name} for di_ in li_2]
        return dic_

    def select_all_response(self):
        res_db = Qresponse().select().where(Qresponse.state == self.id)
        li_ = []
        for dic_ in res_db:
            li_.append({"id": dic_.id, "content": dic_.content, "like": dic_.like, "view_count": dic_.view_count,
                        "comment_count": dic_.comment_count,
                        "user_info": {"username": dic_.users.username, "img_name": dic_.users.img_name}})
        return li_


class MyQresponset:
    def __init__(self, id_=None, content=None, state=0, users=None):
        self.id = id_
        self.content = content
        self.state = state
        self.users = users

    def insert_(self):
        try:
            id_q = Qresponset().insert(content=self.content, state=self.state, users=self.users).execute()
            return id_q
        except Exception as e:
            print(e)
            return False

    def select_s_jadid(self):
        dic_ = {}
        list_db = Qresponset().select().order_by(Qresponset.id.desc())
        for dic2_ in list_db:
            i = str(dic2_.id)
            dic_[i] = {}

            dic_[i]['content'] = dic2_.content
            dic_[i]['user_info'] = {"username": dic2_.users.username, "img_name": dic2_.users.img_name,
                                    "id": dic2_.users.id}
            dic_[i]['list'] = []
            li_2 = Qtagst().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtagst.tags)).where(
                Qtagst.qresponset == int(i))
            dic_[i]['list'] = [di_.tags.name for di_ in li_2]
        return dic_

    def select_s_one(self):
        dic_ = {}
        list_db = Qresponset().get(Qresponset.id == self.id)
        i = str(list_db.id)
        dic_[i] = {}
        dic_[i]['content'] = list_db.content
        dic_[i]['list'] = []
        li_2 = Qtagst().select().join(Tags, JOIN_INNER, on=(Tags.id == Qtagst.tags)).where(
            Qtagst.qresponset == int(i))
        for di_ in li_2:
            dic_[i]['list'].append(di_.tags.name)
        return dic_

    def delete_(self):
        try:
            Qresponset().delete().where(Qresponset.id == self.id).execute()
            return True
        except:
            return False


class MyTags:
    def __init__(self, id_=None, name=None):
        self.id = id_
        self.name = name

    def search_(self):
        try:
            li_ = Tags().get(Tags.name == self.name)
            return li_.id
        except:
            return False

    def create_(self):
        try:
            res_ = Tags().insert(name=self.name).execute()
            return res_
        except:
            return False

    def select_firsr_five(self):
        li_db = Tags().select().paginate(page=1, paginate_by=5)
        li_ = []
        for dic_ in li_db:
            li_.append(dic_.name)
        return li_


class MyQtags:
    def __init__(self, id_=None, qresponse=None, tags=None):
        self.id = id_
        self.qresponse = qresponse
        self.tags = tags

    def insert_(self):
        try:
            Qtags().insert(qresponse=self.qresponse, tags=self.tags).execute()
            return True
        except:
            return False


class MyQtagst:
    def __init__(self, id_=None, qresponset=None, tags=None):
        self.id = id_
        self.qresponset = qresponset
        self.tags = tags

    def insert_(self):
        try:
            Qtagst().insert(qresponset=self.qresponset, tags=self.tags).execute()
            return True
        except:
            return False

    def delete_(self):
        Qtagst().delete().where(Qtagst.qresponset == self.qresponset).execute()
        return True


class Myprovince:
    def __init__(self, id_=None, name=None):
        self.id_ = id_
        self.name = name

    def select(self):
        li_ = Province().select()
        return li_


class MyLikes:
    def __init__(self, id_=None, users=None, qresponse=None, state=None):
        self.id_ = id_
        self.users = users
        self.qresponse = qresponse
        self.state = state

    def search(self):
        try:
            Likes().get(users=self.users, qresponse=self.qresponse, state=self.state)
            return True
        except:
            return False

    def insert(self):
        Likes().insert(users=self.users, qresponse=self.qresponse, state=self.state).execute()
        return True
