from pyramid.view import (view_config)
from models.driver import *
from pyramid.httpexceptions import HTTPFound
import json
import hashlib


class StackOverflow:
    def __init__(self, request):
        self.request = request

    @view_config(renderer='templates/sign up.jinja2', route_name='sign up', request_method="GET")
    def get_sign_up(self):
        o_province = Myprovince()
        res_ = o_province.select()
        return {"active": "signup", "list_": res_}

    @view_config(renderer='json', route_name='sign up', request_method="POST")
    def post_signup(self):
        password = self.request.params['password']
        username = self.request.params['username']
        province = self.request.params['province']
        message = ""
        url = ""
        if username and password and (province != "0"):
            password2 = self.request.params['password2']
            if password == password2:
                name = self.request.params['name']
                family = self.request.params['family']
                email = self.request.params['email']
                img = self.request.params['img']
                try:
                    img_name = self.request.storage.save(img, extentions=('jpg', 'jpeg', 'png', 'gif'))
                except:
                    img_name = '1.jpg'
                o_users = MyUser(username=username, password=password, name=name, family=family, email=email,
                                 img_name=img_name, province=province)
                res_users = o_users.insert_user()
                if res_users:
                    o_Myusertype = MyUsertype(type=2, users=res_users)
                    res_usertype = o_Myusertype.insert()
                    if res_users and res_usertype:
                        self.request.session['user_id'] = res_users
                        self.request.session['img_name'] = img_name
                        self.request.session['username'] = username
                        url = '/student'
                    else:
                        message = 'طلاعات ثبت نشد'
                else:
                    message = "نام کاربری از قبل وجود دارد"
            else:
                message = 'پسورد و تکرار آن با هم برابر نیستند'
        else:
            message = 'پر کردن فیلد های نام کاربری و رمز عبور و استان  الزامی میباشد'
        return {'message': message, 'url': url}

    @view_config(renderer='templates/login.jinja2', route_name='login', request_method="GET")
    def get_login(self):

        return {'bool': '', "active": "login"}

    @view_config(renderer='templates/login.jinja2', route_name='login', request_method="POST")
    def post_login(self):
        username = self.request.params['username']
        password = self.request.params['password']
        object1 = MyUser(username=username, password=password)
        response = object1.check_user()
        if response:
            self.request.session['user_id'] = response['id']
            self.request.session['img_name'] = response['img_name']
            self.request.session['username'] = response['username']
            o_usertype = MyUsertype(users=response['id'])
            res_usertype = o_usertype.select_types_user()
            if len(res_usertype) == 1:
                if res_usertype[0]["user_type"] == "admin":
                    return HTTPFound(location='/admin')
                elif res_usertype[0]['user_type'] == "student":
                    return HTTPFound(location='/student')
                else:
                    return HTTPFound(location="/")
            else:
                self.request.session['user_types'] = res_usertype
                return HTTPFound(location="/choosepanel")
        else:
            return {'bool': 'نام کاربری یا رمز عبور اشتباه است', "active": "login"}

    @view_config(renderer='templates/choosepanel.jinja2', route_name='choosepanel', request_method="GET")
    def get_choose_panel(self):
        user_types = self.request.session['user_types']
        user_types = [type['user_type'] for type in user_types]
        print(user_types)
        return {"li_": user_types}

    @view_config(renderer='templates/student/home.jinja2', route_name='student', request_method="GET")
    def get_student(self):
        o_tags = MyTags()
        list_ = o_tags.select_firsr_five()
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        return {"img_name": img_name, "username": username, "li_": list_}

    @view_config(renderer='json', route_name='student')
    def post_student_1(self):
        user_id = self.request.session['user_id']
        content = self.request.params["content"]
        tags_ = self.request.params["tags"]
        li_ = tags_.split(",")
        o_qrt = MyQresponset(content=content, users=user_id, state=0)
        res_qrt = o_qrt.insert_()
        bool_ = ""
        for name in li_:
            o_tags = MyTags(name=name)
            res_tags1 = o_tags.search_()
            if res_tags1:
                o_qtt = MyQtagst(qresponset=res_qrt, tags=res_tags1)
                res_qtt = o_qtt.insert_()
                if res_qtt:
                    bool_ = "سوال با موفقیت ذخیره شد"
                else:
                    bool_ = "سوال ذخیره نشد ، خطای دیتا بیس"
            else:
                res_tags2 = o_tags.create_()
                o_qtt = MyQtagst(qresponset=res_qrt, tags=res_tags2)
                res_qtt = o_qtt.insert_()
                if res_tags2 and res_qtt:
                    bool_ = "سوال با موفقیت ذخیره شد"
                else:
                    bool_ = "سوال ذخیره نشد ، خطای دیتا بیس"
        return {"bool_": bool_}

    @view_config(renderer='templates/admin/home.jinja2', route_name='admin', request_method="GET")
    def get_home_admin(self):
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        o_qrt = MyQresponset()
        dict_ = o_qrt.select_s_jadid()
        return {"dic_": dict_, "img_name": img_name, "username": username}

    @view_config(renderer='json', route_name='admin', request_method="POST")
    def post_home_admin(self):
        list_info = self.request.params['list_info']
        list_info = json.loads(list_info)
        for dic_ in list_info:
            ob1 = MyQresponse(content=dic_['content'], like=0, state=0, users=dic_['user_id'])
            res1_ = ob1.insert_()
            bool_ = True
            print(dic_['tags'])
            tags = dic_['tags'][0].split(",")
            for name in tags:
                ob2 = MyTags(name=name)
                res2_ = ob2.search_()
                if res2_:
                    ob3 = MyQtags(qresponse=res1_, tags=res2_)
                    res3_ = ob3.insert_()

                else:
                    ob4 = MyTags(name=name)
                    res4_ = ob4.create_()
                    ob5 = MyQtags(qresponse=res1_, tags=res4_)
                    res5_ = ob5.insert_()

            if bool_:
                object_ = MyQtagst(qresponset=int(dic_['id_q']))
                re1_ = object_.delete_()
                object_ = MyQresponset(id_=int(dic_['id_q']))
                re2_ = object_.delete_()

                if re1_ and re2_:
                    message = "سوال با موفقیت ذخیره شد"
                else:
                    message = "no"
            else:
                message = "سوال با موفقیت ذخیره نشد"
        return {"message": message}

    @view_config(renderer='templates/home.jinja2', route_name='home', request_method="GET")
    def get_home(self):
        o_q_response = MyQresponse()
        res = o_q_response.select_s_mahbob()
        res2 = o_q_response.select_s_jadid()
        return {"dic_": res, "dic_2": res2, "active": "home"}

    @view_config(renderer='templates/student/user_edit.jinja2', route_name='user_edit', request_method="GET")
    def get_user_edit(self):
        user_id = self.request.session['user_id']
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        o_users = MyUser(id_=user_id)
        dict_ = o_users.select_user_info()
        return {"dict_": dict_, "img_name": img_name, "username": username}

    @view_config(renderer='json', route_name='user_edit')
    def post_user_edit(self):
        user_id = self.request.session['user_id']
        name = self.request.params['name']
        family = self.request.params['family']
        username = self.request.params['username']
        password = self.request.params['password']
        password2 = self.request.params['password2']
        email = self.request.params['email']
        if username and password:
            if password == password2:
                o_users = MyUser(name=name, family=family, username=username, password=password, email=email,
                                 id_=user_id)
                res_ = o_users.update_user_info()
                if res_:
                    message = "اطلاعات با موفقیت ویرایش شد"
                else:
                    message = "اطلاعات ذخیره نشد ، خطای دیتا بیس"
            else:
                message = "رمز عبور و تکرار آن با هم برابر نیستند"
        else:
            message = "لطفا فیلدهای نام کاربری و رمز عبور را پر کنید"
        return {"message": message}

    @view_config(renderer='templates/admin/admin_edit.jinja2', route_name='admin_edit', request_method="GET")
    def get_admin_edit(self):
        user_id = self.request.session['user_id']
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        o_users = MyUser(id_=user_id)
        dict_ = o_users.select_user_info()
        return {"dict_": dict_, "img_name": img_name, "username": username}

    @view_config(renderer='json', route_name='admin_edit')
    def post_post_edit(self):
        user_id = self.request.session['user_id']
        name = self.request.params['name']
        family = self.request.params['family']
        username = self.request.params['username']
        password = self.request.params['password']
        password2 = self.request.params['password2']
        email = self.request.params['email']
        if username and password:
            if password == password2:
                o_users = MyUser(name=name, family=family, username=username, password=password, email=email,
                                 id_=user_id)
                res_ = o_users.update_user_info()
                if res_:
                    message = "اطلاعات با موفقیت ویرایش شد"
                else:
                    message = "اطلاعات ذخیره نشد ، خطای دیتا بیس"
            else:
                message = "رمز عبور و تکرار آن با هم برابر نیستند"
        else:
            message = "لطفا فیلدهای نام کاربری و رمز عبور را پر کنید"
        return {"message": message}

    @view_config(renderer='templates/home.jinja2', route_name='logout', request_method="GET")
    def get_logout(self):
        # print(self.request.session.delete)
        for key in dict(self.request.session):
            del self.request.session[key]
        return HTTPFound(location="/")

    @view_config(renderer='templates/student/question_show.jinja2', route_name='question_show', request_method="GET")
    def get_question_show(self):
        user_id = self.request.session['user_id']
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        o_qresponse = MyQresponse()
        res = o_qresponse.select_info_5_jadid()
        return {"dic_": res, "user_id": user_id, "img_name": img_name, "username": username}

    @view_config(route_name='question_show', renderer='json')
    def post_question_show(self):
        button = self.request.params['button']
        q_id = self.request.params['q_id']
        like_count = self.request.params['like_count']
        like_count = int(like_count)
        user_id = self.request.session['user_id']
        if button == "like":
            o_likes = MyLikes(users=user_id, qresponse=q_id,
                              state=1)
            res_likes = o_likes.search()
            if res_likes:
                return {"message": False}
            else:
                res_likes = o_likes.insert()
                if res_likes:
                    o_qresponse = MyQresponse(like=(like_count + 1), id_=q_id)
                    res_ = o_qresponse.update_likes()
                    if res_:
                        return {"message": True, "number": (like_count + 1)}
                    else:
                        return {"message": False}
                else:
                    return {"message": False}
        else:
            o_dislikes = MyLikes(users=user_id, qresponse=q_id, state=0)
            res_likes = o_dislikes.search()
            if res_likes:
                return {"message": False}
            else:
                res_likes = o_dislikes.insert()
                if res_likes:
                    o_qresponse = MyQresponse(like=(like_count - 1), id_=q_id)
                    res_ = o_qresponse.update_likes()
                    if res_:
                        return {"message": True, "number": (like_count - 1)}
                    else:
                        return {"message": False}
                else:
                    return {"message": False}

    @view_config(renderer='templates/student/response.jinja2', route_name='response', request_method="GET")
    def get_response(self):
        user_id = self.request.session['user_id']
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        id_q = self.request.matchdict['id_q']
        o_qresponse = MyQresponse(id_=id_q)
        res = o_qresponse.select_one()
        q_response = o_qresponse.select_all_response()
        return {"dic_": res, "user_id": user_id, "img_name": img_name, "username": username, "li_": q_response}

    @view_config(route_name='response', renderer='json')
    def post_response(self):
        button = self.request.params['button']
        q_id = self.request.params['q_id']
        like_count = self.request.params['like_count']
        like_count = int(like_count)
        user_id = self.request.session['user_id']
        if button == "like":
            o_likes = MyLikes(users=user_id, qresponse=q_id, state=1)
            res_likes = o_likes.search()
            if res_likes:
                return {"message": False}
            else:
                res_likes = o_likes.insert()
                if res_likes:
                    o_qresponse = MyQresponse(like=(like_count + 1), id_=q_id)
                    res_ = o_qresponse.update_likes()
                    if res_:
                        return {"message": True, "number": (like_count + 1)}
                    else:
                        return {"message": False}
                else:
                    return {"message": False}
        else:
            o_dislikes = MyLikes(users=user_id, qresponse=q_id, state=0)
            res_likes = o_dislikes.search()
            if res_likes:
                return {"message": False}
            else:
                res_likes = o_dislikes.insert()
                if res_likes:
                    o_qresponse = MyQresponse(like=(like_count - 1), id_=q_id)
                    res_ = o_qresponse.update_likes()
                    if res_:
                        return {"message": True, "number": (like_count - 1)}
                    else:
                        return {"message": False}
                else:
                    return {"message": False}

    @view_config(renderer='templates/student/answer.jinja2', route_name='answer', request_method="GET")
    def get_answer(self):
        user_id = self.request.session['user_id']
        img_name = self.request.session['img_name']
        username = self.request.session['username']
        id_q = self.request.matchdict['id_q']
        o_qresponse = MyQresponse(id_=id_q)
        res = o_qresponse.select_one()
        return {"dic_": res, "user_id": user_id, "img_name": img_name, "username": username}

    @view_config(renderer='json', route_name='answer')
    def post_student(self):
        user_id = self.request.session['user_id']
        content = self.request.params["content"]
        id_q = self.request.params["id_q"]
        o_qrt = MyQresponse(content=content, users=user_id, state=id_q)
        res_qrt = o_qrt.insert_()
        if res_qrt:
            o_qrt.update_comments(id_q)
            return {"message": "پاسخ با موفقیت ذخیره شد"}
        else:
            return {"message": "ذخیره نشد،خطای دیتا بیس"}

    @view_config(renderer='templates/users.jinja2', route_name='users', request_method="GET")
    def get_users(self):
        o_users = MyUser()
        res_users = o_users.select_all()
        return {"active2": "users", "li_": res_users}

    @view_config(renderer='templates/ask_question.jinja2', route_name='ask_question', request_method="GET")
    def get_ask_question(self):

        return {"active2": "askquestion"}

    @view_config(renderer='templates/about.jinja2', route_name='about', request_method="GET")
    def get_about(self):

        return {"active": "about"}

    @view_config(renderer='templates/questions.jinja2', route_name='questions', request_method="GET")
    def get_questions(self):
        o_q_response = MyQresponse()
        res = o_q_response.select_s_all()
        return {"active2": "questions", "dic_": res}
